#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Ici un peu de documentation sur ce programe python
Plus de doc
"""

__author__ = 'Matéo Melki'


def is_valid(adn):
	for i in adn:
		if i not in ["a","t","c","g"]:
			return False
		return True

def get_valid_adn(prompt='chaîne : '):
	adn=input("Rentrez une chaine ADN")
	if is_valid(adn)==False:
		get_valid_adn()

